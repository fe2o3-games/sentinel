//! Generatore di livelli

extern crate rand;
use self::rand::Rng;

type Height = f32;

const HEIGHTS_CNT: Height = 64.;
const TREES_SPARSENESS: u8 = 4;

#[derive(Clone, Debug)]
pub enum Object {
    Empty,
    Tree,
    Sentinel,
    Player
}

// Height is signed. The map starts with all tiles at zero height. Heights are
// changed with square-diamond algorithm, and then remapped to have all heights
// above zero.
#[derive(Clone, Debug)]
pub struct Tile {
    pub x: u16,
    pub y: u16,
    pub height: f32,
    pub content: Object
}

pub struct World {
    pub tiles: Vec<Tile>,
}

// TODO: use World
pub type Map = Vec<Vec<Tile>>;


fn diamond_step(map: &mut Map, x: usize, y: usize, sz: usize, hvar: Height, rng: &mut rand::StdRng) {
    println!("  diamond_step: x={} y={} sz={} hvar={}", x, y, sz, hvar);
    let cx = x + sz/2;
    let cy = y + sz/2;
    let ch = (map[x][y].height + map[x+sz-1][y].height
            + map[x][y+sz-1].height + map[x+sz-1][y+sz-1].height) / 4.;
    map[cx][cy].height = ch;
    if hvar != 0. {
        map[cx][cy].height += (rng.gen::<Height>() - 0.5) * hvar;
    }
    println!("    vals: {} {} {} {}", map[x][y].height, map[x+sz-1][y].height,
                                      map[x][y+sz-1].height, map[x+sz-1][y+sz-1].height);
    println!("    height: {}", map[cx][cy].height);
}

fn square_step(map: &mut Map, x: usize, y: usize, sz: usize, hvar: Height, rng: &mut rand::StdRng) {
    println!("  square_step: x={} y={} sz={} hvar={}", x, y, sz, hvar);
    let cx = x + sz/2;
    let cy = y + sz/2;
    //println!("  square_step: center is {},{}", cx, cy);
    for &(hx, hy) in [(x, cy),  (cx, y),  (cx, y + sz-1), (x + sz-1, cy)].iter() {
        //println!("  square_step: square side center {},{}", hx, hy);
        let mut sum = 0.;
        let mut cnt = 0;
        for &(xv, yv) in [(-1, 0), (0, -1), (0, 1), (1, 0)].iter() {
            let xo = hx as isize + ((sz as isize) / 2 * xv);
            let yo = hy as isize + ((sz as isize) / 2 * yv);

            //println!("    xo={} yo={}", xo, yo);
            if xo >= 0 && xo < map.len() as isize && yo >= 0 && yo < map[xo as usize].len() as isize {
                //println!("      ok");
                if map[xo as usize][yo as usize].height != 0. {
                    sum += map[xo as usize][yo as usize].height;
                    println!("    val {}", map[xo as usize][yo as usize].height);
                    cnt += 1;
                }
            }
        }
        map[hx][hy].height = sum / cnt as Height;
        if hvar != 0. {
            map[hx][hy].height += (rng.gen::<Height>() - 0.5) * hvar
        }
        println!("    cnt: {} - height: {}", cnt, map[hx][hy].height);
    }
}

/// Recursively change heigths in a map using diamond square algorithm.
///
/// * `map` - complete map
/// * `x` - x coordinate of the map portion to change heights of
/// * `y` - y coordinate of the map portion to change heights of
/// * `sz` - size of the side of the square map portion to change.
///          Note: has to be 2^n+1
/// * `hvar` - range of the height change, i.e. increase this value to obtain
///            steeper and more dramatic landscapes
///
fn diamond_square(map: &mut Map, x: usize, y: usize, sz: usize, hvar: Height, rng: &mut rand::StdRng) {
    println!("diamond_square: x={} y={} sz={} hvarl={}", x, y, sz, hvar);

    diamond_step(map, x, y, sz, hvar, rng);
    square_step(map, x, y, sz, hvar, rng);

    let next_sz = sz / 2 + 1;
    if next_sz < 3 {
        return;
    }
    let next_hvar = hvar * 0.6;

    diamond_square(map, x, y, next_sz, next_hvar, rng);
    diamond_square(map, x + next_sz-1, y, next_sz, next_hvar, rng);
    diamond_square(map, x, y + next_sz-1, next_sz, next_hvar, rng);
    diamond_square(map, x + next_sz-1, y + next_sz-1, next_sz, next_hvar, rng);
}

fn change_map_height(map: &mut Map, hdelta: Height) {
    for x in 0..map.len() {
        for y in 0..map[x].len() {
            map[x][y].height += hdelta;
        }
    }
}

fn find_min_max(map: &Map) -> ((usize, usize), (usize, usize)) {
    let mut lowest_cell = (0, 0);
    let mut highest_cell = (0, 0);
    let mut min_height = HEIGHTS_CNT;
    let mut max_height = 0.;

    for x in 0..map.len() {
        for y in 0..map[x].len() {
            let h = map[x][y].height;
            if h < min_height {
                min_height = h;
                lowest_cell = (x, y);
            }
            if h > max_height {
                max_height = h;
                highest_cell = (x, y);
            }
        }
    }
    (lowest_cell, highest_cell)
}

fn genobjects(map: &mut Map, rng: &mut rand::StdRng) {
    for x in 0..map.len() {
        for y in 0..map[x].len() {
            if rng.gen::<u8>() % TREES_SPARSENESS == 0 {
                map[x][y].content = Object::Tree;
            }
        }
    }

    let (lowest_cell, highest_cell) = find_min_max(map);
    map[lowest_cell.0][lowest_cell.1].content = Object::Player;
    map[highest_cell.0][highest_cell.1].content = Object::Sentinel;
}


/// Generate world map using.
///
/// Uses diamond square algorithm to generate heights, add randomly positioned
/// trees, and put a sentinel in the higher tile, and player in the lower one.
///
/// * `order` - power of two for the map size. The map will be a square with
///             a side 2^n+1 long.
/// * `rng` - random number generator object
///
pub fn generate(order: u32, mut rng: &mut rand::StdRng) -> World {
    let mut map: Map = Vec::new();
    let size: usize = (2u8.pow(order) + 1) as usize;

    for x in 0..size {
        map.push(Vec::new());

        for y in 0..size {
            map[x].push(Tile {
                height: 0.,
                x: x as u16,
                y: y as u16,
                content: {
                    if rng.gen::<u8>() % TREES_SPARSENESS == 0 {
                        Object::Tree
                    } else {
                        Object::Empty
                    }
                }
            });
        }
    }

    map[0][0].height = rng.gen::<Height>() * HEIGHTS_CNT;
    map[0][size-1].height = rng.gen::<Height>() * HEIGHTS_CNT;
    map[size-1][0].height = rng.gen::<Height>() * HEIGHTS_CNT;
    map[size-1][size-1].height = rng.gen::<Height>() * HEIGHTS_CNT;

    diamond_square(&mut map, 0, 0, size, HEIGHTS_CNT / 2., rng);

    let (lowest_cell, _) = find_min_max(&map);
    let h = map[lowest_cell.0][lowest_cell.1].height;
    change_map_height(&mut map, -h);

    genobjects(&mut map, &mut rng);

    World::new(map.concat())
}


#[allow(dead_code)]
pub fn dump(map: Map) {
    for row in map {
        for tile in row {
            print!("{:02}", tile.height);
            match tile.content {
                Object::Empty => print!("-"),
                Object::Tree => print!("A"),
                Object::Sentinel => print!("s"),
                Object::Player => print!("i")
            }
            print!(" ");
        }
        print!("\n");
    }
}
