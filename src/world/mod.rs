extern crate std;
extern crate cgmath;

use std::iter::Iterator;
use cgmath::{Point3, Vector3, InnerSpace};

use map::{Tile, World};

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub position: (f32, f32, f32),
    pub color: (f32, f32, f32),
    pub pos: (i32, i32),
}
implement_vertex!(Vertex, position, color, pos);

#[derive(Copy, Clone, Debug)]
pub struct Normal {
    pub normal: (f32, f32, f32),
}

pub type Index = u16;

implement_vertex!(Normal, normal);

impl Vertex {
    fn ground(&self) -> Vertex {
        let mut v = self.clone();
        v.position.1 = 0.0;  // move to the ground
        v
    }
}

enum Side {
    N,
    S,
    W,
    E
}

enum Dir {
    Top,
    Side(Side)
}

type Quad = [Vertex; 4];
type Para = Vec<Vertex>;
type Norm = Vec<Normal>;
type Indx = Vec<Index>;

impl Tile {
    fn vertices(&self) -> Para {
        let top = self._top();
        let n = self._side(top, Side::N);
        let s = self._side(top, Side::S);
        let w = self._side(top, Side::W);
        let e = self._side(top, Side::E);
        [ &top[..]
        , &n[..]
        , &s[..]
        , &w[..]
        , &e[..]
        ].concat()  // 4*5 = 20 vertici
    }
    fn _top(&self) -> Quad {
        let height = self.height as f32 / 4.0;
        let x1 = self.x as f32;
        let x2 = x1 + 1.0;
        let z1 = self.y as f32;
        let z2 = z1 + 1.0;
        let b = height / 8.;
        [
            Vertex{position: (x1, height, z1), color: (1.0, b, b), pos: (self.x as i32, self.y as i32)},
            Vertex{position: (x1, height, z2), color: (0.0, b, b), pos: (self.x as i32, self.y as i32)},
            Vertex{position: (x2, height, z2), color: (0.0, b, b), pos: (self.x as i32, self.y as i32)},
            Vertex{position: (x2, height, z1), color: (0.5, b, b), pos: (self.x as i32, self.y as i32)},
        ]
    }
    fn _side(&self, top: Quad, side: Side) -> Quad {
        let (i1, i2) = match side {
            Side::N => (3, 0),
            Side::S => (1, 2),
            Side::W => (0, 1),
            Side::E => (2, 3)
        };
        let a = top[i1];
        let d = top[i2];
        let b = a.ground();
        let c = d.ground();
        [a, b, c, d]
    }
    fn _normals(&self, dir: Dir) -> [Normal; 4] {
        let normal = match dir {
            Dir::Top           => ( 0.0, 1.0,  0.0),
            Dir::Side(Side::N) => ( 0.0, 0.0, -1.0),
            Dir::Side(Side::S) => ( 0.0, 0.0,  1.0),
            Dir::Side(Side::W) => (-1.0, 0.0,  0.0),
            Dir::Side(Side::E) => ( 1.0, 0.0,  0.0)
        };
        let n = Normal {normal: normal};
        [n, n, n, n]
    }
    fn normals(&self) -> Norm {
        [ &self._normals(Dir::Top)[..]
         , &self._normals(Dir::Side(Side::N))[..]
         , &self._normals(Dir::Side(Side::S))[..]
         , &self._normals(Dir::Side(Side::W))[..]
         , &self._normals(Dir::Side(Side::E))[..]
        ].concat()

    }
    fn _indices(&self, start: Index) -> [Index; 6] {
        let s = start;
        [s+0, s+1, s+2,
         s+0, s+2, s+3]
    }
    fn indices(&self, n: u16) -> Indx {
        // 4 vertices * 5 sides
        let start = n * 4 * 5;
        [ &self._indices(start + 0)[..]
         , &self._indices(start + 4)[..]
         , &self._indices(start + 8)[..]
         , &self._indices(start + 12)[..]
         , &self._indices(start + 16)[..]
        ].concat()
    }
    pub fn center(&self) -> Point3<f32> {
        Point3::new(self.x as f32 + 0.5, self.height / 4.0, self.y as f32 + 0.5)
    }
    fn intersect(&self, o: Point3<f32>, d: Vector3<f32>) -> Option<f32> {
        let x = self.center();
        let b = d.dot(o - x);
        let c = (o - x).dot(o - x) - 0.5*0.5;
        if b*b - c >= 0.0 {
            let s = (b*b - c).sqrt();
            Some(f32::min(-b + s, -b - s))
        } else {
            None
        }
    }

    pub fn is_inside(&self, p: Point3<f32>) -> bool {
        (p.x >= self.x as f32) &&
        (p.x <= self.x as f32 + 1.0) &&
        (p.z >= self.y as f32) &&
        (p.z <= self.y as f32 + 1.0)
    }
}


impl World {
    pub fn new(tiles: Vec<Tile>) -> World {
        World {tiles: tiles}
    }
    pub fn vertices(&self) -> Vec<Vertex> {
        let mut vertices = Vec::new();
        for tile in self.tiles.iter() {
            for vertex in tile.vertices().iter() {
                vertices.push(vertex.clone());
            }
        }
        vertices
    }
    pub fn normals(&self) -> Vec<Normal> {
        let mut normals = Vec::new();
        for tile in self.tiles.iter() {
            for normal in tile.normals().iter() {
                normals.push(normal.clone());
            }
        }
        normals
    }
    pub fn indices(&self) -> Vec<Index> {
        let mut indices = Vec::new();
        for (i, tile) in self.tiles.iter().enumerate() {
            indices.extend(tile.indices(i as u16));
        }
        indices
    }
    pub fn intersect(&self, o: Point3<f32>, d: Vector3<f32>) -> Option<&Tile> {
        let mut nearest = None;
        for tile in self.tiles.iter() {
            if let Some(t) = tile.intersect(o, d) {
                match nearest {
                    Some((tmin, _)) if tmin < t => {},
                    _ => { nearest = Some((t, tile)) },
                }
            }
        }
        if let Some((_, tile)) = nearest {
            Some(tile)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use map::{Tile, World, Object};
    use cgmath::Point3;

    impl Tile {
        fn new(x: u16, y: u16, height: f32) -> Tile {
            Tile { x: x, y: y, height: height, content: Object::Empty }
        }
    }

    #[test]
    fn test_tile_vertices() {
        let tile = Tile::new(0, 0, 1.0);
        assert_eq!(tile.vertices().len(), 4 * 5);
    }

    #[test]
    fn test_world_vertices() {
        let tile = Tile::new(0, 0, 1.0);
        let world = World::new(vec![tile.clone(), tile.clone()]);
        assert_eq!(world.vertices().len(), 2*tile.vertices().len());
    }

    #[test]
    fn test_world_indices() {
        let tile = Tile::new(0, 0, 1.0);
        let world = World::new(vec![tile.clone(), tile.clone()]);
        assert_eq!(world.indices().len(), 2*tile.indices(0).len());
        println!("Indices: {:?}", world.indices());
    }

    #[test]
    fn test_tile_is_inside() {
        let tile = Tile::new(0, 0, 1.0);
        let point = Point3::new(0.5, 1.0, 0.5);
        assert!(tile.is_inside(point));
        let point_outside = Point3::new(-0.1, 1.0, 0.5);
        assert!(tile.is_inside(point_outside) == false);
    }
}
