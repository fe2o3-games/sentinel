//! Modulo di utilità per funzioni relative alla camera.

extern crate cgmath;

use cgmath::{Point3, Vector3, Matrix4, Rad, Deg, Basis3, Rotation3, Rotation, InnerSpace};
use std::f32;

// position: the position of the camera in world space
#[derive(Debug)]
pub struct Camera {
    position: Point3<f32>,
    pitch: Rad<f32>,
    yaw: Rad<f32>,
}

#[derive(Debug, Clone)]
pub enum Look {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Clone)]
pub enum Move {
    Forward,
    Backward,
    Left,
    Right,
}

impl Default for Camera {
    fn default() -> Camera {
        Camera {
            position: Point3::new(0.0, 0.0, 0.0),
            pitch: cgmath::rad(0.0),
            yaw: cgmath::rad(0.0),
        }
    }
}

pub struct ViewProjection<T> {
    pub view: Matrix4<T>,
    pub projection: Matrix4<T>,
    position: cgmath::Point3<T>,
    size: (u32, u32),
}

impl ViewProjection<f32> {
    pub fn ray(&self, mouse: Option<(i32, i32)>) -> Option<(Point3<f32>, Vector3<f32>)> {
        use self::cgmath::{Point3, Vector4, SquareMatrix, InnerSpace};
        let (w, h) = self.size;
        if let Some((mx,my)) = mouse {
            if w == 0 || h == 0 {
                return None;
            }
            let x = 2.0 * (mx as f32 / w as f32) - 1.0;
            let y = 1.0 - 2.0 * (my as f32 / h as f32);
            if let Some(inv_prj) = self.projection.invert() {
                if let Some(inv_view) = self.view.invert() {
                    // Punto sul lato anteriore del normalized-device-space xyz in [-1:1]
                    let ray_clip = Vector4::new(x, y, -1.0, 1.0);
                    // Adesso il punto è nell'eye-space (camera nell'origine)
                    let ray_eye = inv_prj * ray_clip;
                    // Adesso il punto è in coordinate mondo
                    let ray_wor = inv_view * ray_eye;
                    // Per avere la direzione devo usare la posizione della camera
                    let target = Point3::from_homogeneous(ray_wor);
                    return Some((self.position, (target - self.position).normalize()));
                }
            }
        }
        None
    }
}

impl Camera {
    // Canonical camera representation: position at the origin of axes, z-axis pointing inside
    // the camera.
    pub fn new() -> Camera {
        Default::default()
    }

    pub fn new_pos(pos: Point3<f32>) -> Camera {
        Camera {
            position: pos,
            ..Default::default()
        }
    }

    pub fn matrix(&self, size: (u32, u32)) -> ViewProjection<f32> {
        let (width, height) = size;
        let p = cgmath::PerspectiveFov {
                    fovy: cgmath::rad(f32::consts::FRAC_PI_2 / 2.0),
                    aspect: width as f32 / height as f32,
                    near: 0.1,
                    far: 4096.0,
                }.to_perspective();
        ViewProjection {
            view: Matrix4::look_at(self.position, self.center(), Vector3::unit_y()),
            projection: cgmath::frustum(p.left, p.right, p.bottom, p.top, p.near, p.far),
            position: self.position,
            size: size,
        }
    }

    pub fn look_at(&mut self, pitch: Rad<f32>, yaw: Rad<f32>) {
        self.pitch = pitch;
        self.yaw = yaw;
    }

    pub fn look_delta(&mut self, look_delta: Option<Look>) {
        if let Some(direction) = look_delta {
            let zero = Rad::new(0.0);
            let step = Rad::from(Deg::new(1.0));
            let mut pitch_delta = zero;
            let mut yaw_delta = zero;
            match direction {
                Look::Up    => { pitch_delta = -step; },
                Look::Down  => { pitch_delta = step; },
                Look::Left  => { yaw_delta = step; },
                Look::Right => { yaw_delta = -step; },
            }
            let pitch = self.pitch - pitch_delta;  // up
            let yaw = self.yaw + yaw_delta;  // side
            let max_pitch: Rad<f32> = Deg::new(70.0).into();
            if -max_pitch < pitch && pitch <  max_pitch {
                self.look_at(pitch, yaw);
            }
        }
    }

    fn center(&self) -> Point3<f32> {
        let rot = Basis3::<_>::from_euler(self.pitch, self.yaw, cgmath::rad(0.0));
        let target = rot.rotate_vector(-Vector3::unit_z());
        self.position + target
    }

    pub fn move_delta(&mut self, move_delta: Option<Move>) {
        // When moving, we must compute the translation relative to the center of the camera.
        // First compute unit vectors in the forward and lateral directions, then compute the
        // translation using provided deltas.
        // Reverse the forward delta because z-axis points into the camera.
        if let Some(direction) = move_delta {
            let mut lateral_delta = 0.0;
            let mut forward_delta = 0.0;
            match direction {
                Move::Forward => { forward_delta = 0.1; },
                Move::Backward => { forward_delta = -0.1; },
                Move::Left => { lateral_delta = -0.1; },
                Move::Right => { lateral_delta = 0.1; },
            }
            let center_vec = (self.center() - self.position).normalize();
            let lateral_vec = center_vec.cross(Vector3::unit_y()).normalize();
            self.position += (center_vec * forward_delta) + (lateral_vec * lateral_delta);
        }
    }
    pub fn set_position(&mut self, position: Point3<f32>) {
        self.position = position;
    }

    pub fn position(&self) -> Point3<f32> {
        self.position
    }
}
