#[macro_use]
extern crate glium;
extern crate sentinel;
extern crate cgmath;
extern crate rand;
extern crate obj;

extern crate ears;
use ears::{Sound, AudioController};


use std::fs::File;
use std::io::BufReader;
use std::time::Instant;
use std::time::Duration;

use rand::{SeedableRng, StdRng};
use obj::{Obj,load_obj};

mod map;
mod world;

fn convert_object(object:& Obj) -> (Vec<world::Vertex>, Vec<world::Normal>) {
    let mut vertices = Vec::new();
    let mut normals = Vec::new();
    for v in &object.vertices {
        let vpos = (v.position[0], v.position[1], v.position[2]);
        let npos = (v.normal[0], v.normal[1], v.normal[2]);
        vertices.push(world::Vertex{ position: vpos, color: (0.5,0.6,0.3), pos: (0, 0) });
        normals.push(world::Normal{ normal: npos })
    }
    (vertices, normals)
}

enum Mouse {
    Drag,
    Click,
    Hover,
}

fn main() {
    use glium::{DisplayBuild, Surface};
    use glium::index::PrimitiveType::TrianglesList;
    use sentinel::camera::{Camera, Look, Move};

    let display = glium::glutin::WindowBuilder::new()
                        .with_depth_buffer(24)
                        .with_dimensions(640, 480)
                        .build_glium().unwrap();

    // let seed: &[_] = &[1, 2, 3, 4];
    // let mut rng = SeedableRng::from_seed(seed);
    let mut rng = StdRng::new().unwrap();
    let terrain = map::generate(5, &mut rng);
    let vertices = glium::VertexBuffer::new(&display, &terrain.vertices()).unwrap();
    let normals = glium::VertexBuffer::new(&display, &terrain.normals()).unwrap();
    let indices = glium::IndexBuffer::new(&display, TrianglesList,
                                          &terrain.indices()).unwrap();

    let mut tmp = None;
    for tile in &terrain.tiles {
        if let map::Object::Tree = tile.content {
            tmp = Some(tile);
            break;
        }
    };
    let tree_tile = tmp.unwrap();
    let tree_transform: [[f32; 4]; 4] = {
        use cgmath::{Matrix4, Point3, vec3};
        use cgmath::EuclideanSpace;
        let point = tree_tile.center() + vec3(0.0, 1.0, 0.0);
        Matrix4::from_translation((point - Point3::origin()) + vec3(0.0, -1.2, 0.0)).into()
    };

    let mut game_finished = false;
    // Create a new Sound.
    let mut end_sound = Sound::new("trumpet.wav").unwrap();
    let mut teleport_sound = Sound::new("teleport.wav").unwrap();

    // TODO: wrap in another function
    let input = BufReader::new(File::open("alberello.obj").unwrap());
    let dome: Obj = load_obj(input).unwrap();

    let (vtree, ntree) = convert_object(&dome);
    let vb = glium::VertexBuffer::new(&display, &vtree).unwrap();
    let nb = glium::VertexBuffer::new(&display, &ntree).unwrap();
    let ib = glium::IndexBuffer::new(&display, TrianglesList, &dome.indices).unwrap();

    // Shuttle
    let input = BufReader::new(File::open("shuttle.obj").unwrap());
    let shuttle: Obj = load_obj(input).unwrap();

    let (vs, ns) = convert_object(&shuttle);
    let vshuttle = glium::VertexBuffer::new(&display, &vs).unwrap();
    let nshuttle = glium::VertexBuffer::new(&display, &ns).unwrap();
    let ishuttle = glium::IndexBuffer::new(&display, TrianglesList, &shuttle.indices).unwrap();

    let vertex_shader_src = r#"
        #version 150

        in vec3 position;
        in vec3 color;
        in vec3 normal;
        in ivec2 pos;

        out vec3 v_normal;
        out vec3 v_color;

        uniform mat4 view;
        uniform mat4 projection;
        uniform ivec2 u_pos;

        void main() {
            v_normal = transpose(inverse(mat3(view))) * normal;
            gl_Position = projection * view * vec4(position, 1.0);
            if (pos.x == u_pos.x && pos.y == u_pos.y) {
                v_color = color*2;
            } else {
                v_color = color;
            }
        }
    "#;

    let entity_vertex_src = r#"
        #version 150

        in vec3 position;
        in vec3 color;
        in vec3 normal;

        out vec3 v_normal;
        out vec3 v_color;

        uniform mat4 view;
        uniform mat4 projection;
        uniform mat4 translation;

        void main() {
            v_normal = transpose(inverse(mat3(view))) * normal;
            v_color = color;
            gl_Position = projection * view * translation * vec4(position, 1.0);
        }
    "#;
    let fragment_shader_src = r#"
        #version 150

        in vec3 v_normal;
        in vec3 v_color;
        out vec4 color;
        uniform vec3 u_light;
        uniform int fog_enabled;

        void main() {
            float brightness = dot(normalize(v_normal), normalize(u_light));
            float dist = gl_FragCoord.z / gl_FragCoord.w;
            float fogFactor = clamp((10 - dist) / (10 - 1), 0.0, 1.0);
            //FIXME: keep in sync with target.clear_color_and_depth
            vec3 fog_color = vec3(0.0, 0.0, 0.0);
            vec3 dark_color = v_color * 0.6;
            vec3 regular_color = v_color;
            vec3 item_color = mix(dark_color, regular_color, brightness);
            vec3 finalColor = item_color;
            if (fog_enabled == 1) {
                finalColor = mix(fog_color, item_color, fogFactor);
            }
            color = vec4(finalColor, 1.0);
        }
    "#;
    let mut fog_enabled: i32 = 1;

    let program = glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src,
                                              None).unwrap();

    let entity_program = glium::Program::from_source(&display, entity_vertex_src, fragment_shader_src,
                                                     None).unwrap();


     let mut player_tile = None;
     for tile in &terrain.tiles {
         if let map::Object::Player = tile.content {
             player_tile = Some(tile);
             break;
         }
    };
    let player_pos = if let Some(player) = player_tile {
        use cgmath::vec3;
        player.center() + vec3(0.0, 1.0, 0.0)
    } else {
        panic!("player not found")
    };
    let mut mouse = None;
    let mut click = Mouse::Hover;
    let mut camera = Camera::new_pos(player_pos);
    camera.look_at(cgmath::rad(0.0), cgmath::rad(0.0));
    // let mut camera = Camera::new_pos(Point3::new(32.573795, 5.0082784, 29.365202));
    //
    // camera.look_at(cgmath::rad(-0.31415924), cgmath::rad(-11.658747));

    let mut look_delta: Option<Look> = None;
    let mut move_delta: Option<Move> = None;
    loop {
        let light = [-1.0, 0.4, 0.9f32];
        let mut target = display.draw();
        target.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);

        camera.look_delta(look_delta.clone());
        camera.move_delta(move_delta.clone());
        let camera_matrix = camera.matrix(target.get_dimensions());

        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::draw_parameters::DepthTest::IfLess,
                write: true,
                .. Default::default()
            },
            .. Default::default()
        };

        let mut pos = [-1i32, -1i32];
        if let Some((o, d)) = camera_matrix.ray(mouse) {
            if let Some(tile) = terrain.intersect(o, d) {
                pos = [tile.x as i32, tile.y as i32];
            } else {
                pos = [-1i32, -1i32];
            }
        };

        let v: [[f32; 4]; 4] = camera_matrix.view.into();
        let p: [[f32; 4]; 4] = camera_matrix.projection.into();

        match click {
            Mouse::Click => {
                if let Some((o, d)) = camera_matrix.ray(mouse) {
                    if let Some(tile) = terrain.intersect(o, d) {
                        use cgmath::vec3;
                        teleport_sound.play();
                        camera.set_position(tile.center() + vec3(0.0, 1.0, 0.0));
                        // Reset click state to avoid double clicks.
                        click = Mouse::Hover;
                    }
                }
            },
            _ => {},
        }

        if !game_finished {
            game_finished = tree_tile.is_inside(camera.position());
            // play the sound only once when the game is finished
            if game_finished {
                end_sound.play();
                camera.set_position(cgmath::Point3::new(1.8586979, 46.780376, 22.26926));
                camera.look_at(cgmath::rad(-1.40), cgmath::rad(-1.40));
                fog_enabled = 0;
            }
        }

        let draw_start = Instant::now();
        target.draw((&vertices, &normals), &indices, &program,
                    &uniform! { view: v,
                                projection: p,
                                u_light: light,
                                u_pos: pos,
                                fog_enabled: fog_enabled,
                                },
                    &params).unwrap();

        // target.draw((&vb, &nb), &ib, &entity_program,
        //             &uniform! { view: v,
        //                         projection: p,
        //                         u_light: light,
        //                         translation: tree_transform,
        //                         fog_enabled: fog_enabled,
        //                         },
        //             &params).unwrap();
        target.draw((&vshuttle, &nshuttle), &ishuttle, &entity_program,
                    &uniform! { view: v,
                                projection: p,
                                u_light: light,
                                translation: tree_transform,
                                fog_enabled: fog_enabled,
                                },
                    &params).unwrap();

        target.finish().unwrap();

        use glium::glutin::{Event};
        use glium::glutin::ElementState::{Pressed, Released};
        use glium::glutin::VirtualKeyCode::{Up, Down, Left, Right, W, S, A, D};
        for ev in display.poll_events() {
            match ev {
                Event::Closed => { println!("{:?}", camera); return },
                Event::KeyboardInput(Pressed, _, Some(Up))    => { look_delta = Some(Look::Up); },
                Event::KeyboardInput(Pressed, _, Some(Down))  => { look_delta = Some(Look::Down); },
                Event::KeyboardInput(Pressed, _, Some(Left))  => { look_delta = Some(Look::Left); },
                Event::KeyboardInput(Pressed, _, Some(Right)) => { look_delta = Some(Look::Right); },
                Event::KeyboardInput(Pressed, _, Some(W)) => { move_delta = Some(Move::Forward); },
                Event::KeyboardInput(Pressed, _, Some(S)) => { move_delta = Some(Move::Backward); },
                Event::KeyboardInput(Pressed, _, Some(A)) => { move_delta = Some(Move::Left); },
                Event::KeyboardInput(Pressed, _, Some(D)) => { move_delta = Some(Move::Right); },
                Event::KeyboardInput(Released, _, Some(Up))    => { look_delta = None; },
                Event::KeyboardInput(Released, _, Some(Down))  => { look_delta = None; },
                Event::KeyboardInput(Released, _, Some(Left))  => { look_delta = None; },
                Event::KeyboardInput(Released, _, Some(Right)) => { look_delta = None; },
                Event::KeyboardInput(Released, _, Some(W)) => { move_delta = None; },
                Event::KeyboardInput(Released, _, Some(S)) => { move_delta = None; },
                Event::KeyboardInput(Released, _, Some(A)) => { move_delta = None; },
                Event::KeyboardInput(Released, _, Some(D)) => { move_delta = None; },
                Event::MouseMoved(x, y) => { mouse = Some((x, y)) },
                Event::MouseInput(Pressed, _) => { click = Mouse::Drag; },
                Event::MouseInput(Released, _) => {
                    if let Mouse::Drag = click {
                        click = Mouse::Click;
                    }
                }
                _ => ()
            }
            // TODO: understand why this check is needed
            match ev {
                Event::KeyboardInput(..) | Event::MouseMoved(..) => {
                    if let Mouse::Click = click {
                        click = Mouse::Hover;
                    }
                },
                _ => {}
            }
        }

        move_delta = None;
        if game_finished {
            look_delta = None;
            click = Mouse::Hover;
        }

        let draw_time = Instant::now().duration_since(draw_start);
        let fps60_time = Duration::new(0, 16666667); // 1/60 in nanoseconds
        if draw_time < fps60_time {
            std::thread::sleep(fps60_time - draw_time);
        }
    }
}
