This is a reimplementation of the Sentinel game in Rust for DevelHack 2016
competition.

See this video for an example of gameplay of the original game:
https://www.youtube.com/watch?v=9V_pgo3vgiI

Planned features:
* 3d view
* texturing
* automatic level generation
* light effects where the sentinel is looking