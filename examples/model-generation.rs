#[macro_use]
extern crate glium;

use std::fmt;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use std::path::Path;

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub position: (f32, f32, f32),
}
implement_vertex!(Vertex, position);

#[derive(PartialEq, Debug)]
enum SurfaceType {
    Height
}

#[derive(Debug)]
struct Tile {
    surface: SurfaceType,
    x: u16,
    y: u16,
    height: u8,
}

impl Tile {
    fn new_height(row: usize, column: usize, height: u8) -> Tile {
        Tile {
            surface: SurfaceType::Height,
            x: row as u16,
            y: column as u16,
            height: height,
        }
    }
}

fn terrain_generator(world: &Vec<Vec<Tile>>) -> (Vec<Vertex>, Vec<usize>){
    // Now that we have all tiles in our world, we generate triangles
    let mut vertices = Vec::new();
    let mut indices = Vec::new();

    // Basic algorithm
    // - create triangles and indices for current tile
    // - create "fake tiles" horizontally and vertically to link with above and left tiles
    // - create "fake tile" diagonally
    // The resulting diagram is as follows (ff stands for "fake tile", numbers are heights for real tiles).
    //        02 ff 00
    //        ff ff ff
    //        00 ff 00

    // 4 points for each tile
    let stride = world[0].len() * 4;

    for row in 0..world.len() {
        for col in 0..world[0].len() {
            let tile = &world[row][col];
            let top_left_x = col as f32;
            let top_left_z = row as f32;

            let current_top_left_index = row * stride + col * 4;

            // IMPORTANT! Vertices must be inserted in anti clockwise order inside the vertices
            // vector, starting from top left corner.
            vertices.push(Vertex{position: (top_left_x, tile.height as f32, top_left_z)});
            vertices.push(Vertex{position: (top_left_x, tile.height as f32, top_left_z - 1.0)});
            vertices.push(Vertex{position: (top_left_x + 1.0, tile.height as f32, top_left_z - 1.0)});
            vertices.push(Vertex{position: (top_left_x + 1.0, tile.height as f32, top_left_z)});

            indices.append(&mut vec![current_top_left_index, current_top_left_index+1, current_top_left_index+3]);
            indices.append(&mut vec![current_top_left_index+1, current_top_left_index+2, current_top_left_index+3]);

            // generate the triangles for the link slope between this tile and the tile on the left
            if col > 0 {
                let left_index = row * stride + (col - 1) * 4;
                let prev_bottom_right = left_index + 2;
                let prev_top_right = left_index + 3;
                indices.append(&mut vec![prev_top_right, prev_bottom_right, current_top_left_index]);
                indices.append(&mut vec![prev_bottom_right, current_top_left_index+1, current_top_left_index]);
            }

            // generate the triangles for the link slope between this tile and the tile above
            if row > 0 {
                let above_index = (row - 1) * stride + col * 4;
                let above_bottom_left = above_index + 1;
                let above_bottom_right = above_index + 2;
                indices.append(&mut vec![above_bottom_left, current_top_left_index, above_bottom_right]);
                indices.append(&mut vec![current_top_left_index, current_top_left_index+3, above_bottom_right]);
            }

            // generate the triangles for the link slope between this tile and the tile diagonally
            // above-left
            if row > 0 && col > 0 {
                let diag_bottom_right = ((row - 1) * stride + (col - 1) * 4) + 2;
                let left_top_right = (row * stride + (col - 1) * 4) + 3;
                let above_bottom_left = ((row - 1) * stride + col * 4) + 1;
                indices.append(&mut vec![diag_bottom_right, left_top_right, above_bottom_left]);
                indices.append(&mut vec![left_top_right, current_top_left_index, above_bottom_left]);
            }
        }
    }
    (vertices, indices)
}

fn parse_data_file(path: &Path) -> Result<Vec<Vec<Tile>>, std::io::Error>{
    let f = try!(File::open(path));
    let file = BufReader::new(&f);
    let mut row: usize = 0;
    let mut world = Vec::new();
    for line in file.lines() {
        let mut world_row = Vec::new();
        if let Ok(l) = line {
            let mut col = 0;
            for s in l.split_whitespace() {
                let cell = s.as_bytes();
                world_row.push(
                    if cell[0] == '-' as u8{
                        panic!("Slopes tiles are not supported")
                    } else {
                        Tile::new_height(row, col, cell[1] - '0' as u8)
                    }
                );
                col = col + 1;
            }
        }
        row = row + 1;
        world.push(world_row);
    }

    Ok(world)
}

fn main() {
    let res = parse_data_file(Path::new("map1.data"));
    match res {
        // Ok(world) => println!("Tutto ok, {}", world) terrain_generator(&world),
        // Ok(world) => println!("Tutto ok, {:?}", world),
        Ok(world) => {
            let (v, i) = terrain_generator(&world);
            println!("world size {}, {}", world.len(), world[0].len());
            println!("vertices {:?}, indices {:?}", v, i);
        },
        Err(e) => println!("Error parsing data file, {}", e),
    }
}
